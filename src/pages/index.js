import React from "react"

import { withAuthenticator } from 'aws-amplify-react'
import Amplify from 'aws-amplify'

import aws_exports from '../aws-exports'

Amplify.configure(aws_exports)

const App = () => <div>Hello world!</div>
export default withAuthenticator(App)
